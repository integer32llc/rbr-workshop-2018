
background-image: url(./assets/rust-test-pattern.jpg)

---

class: center, middle

# Hands-on introduction to Rust

---

class: bullets-7x7

# Agenda (1/2)

1. Cargo
1. Basics and documentation
1. Iterating
1. Making our own types
1. Strings and user input

---

class: bullets-7x7

# Agenda (2/2)

<ol start="6">
<li>Error handling</li>
<li>Modules</li>
<li>More?</li>
</ol>

---

class: center, middle

<img class="full-image" src="./assets/integer32.png" alt="i32 logo" />

???

- Cofounder

---

class: center, middle

# Stack Overflow

<img class="kirby" src="./assets/kirby.png" alt="gravatar" />

???

- \#1 answerer on Stack Overflow for Rust tag

---

class: center, middle

# Rust Playground

<img class="full-image" src="./assets/playground.png" alt="playground screenshot" />

### [play.rust-lang.org](https://play.rust-lang.org)

???

- maintainer

---

class: bullets-7x7

# Jake Goulding

- Rust infrastructure team
- Working on a Rust video course for Manning
- A handful of crates
- Help out with AVR-Rust

???

- C background
- XML crates, parsing, low-level assembly

---

class: center, middle

# Who are you?

???

- Who is a programmer?
- What languages?
- Used Rust at all?
- Why Rust?

---

class: bullets-7x7

# Cargo

- Package manager
- Build tool
  - Code
  - Tests
  - Docs

---

# Cargo

`cargo new --bin name-of-crate`

```
.
├── Cargo.toml
└── src
    └── main.rs
```

<!-- -->

???

- `bin` is the default
- Can also make reusable libraries
- Have students list the directory

---

# Cargo

`cargo run`

```
.
├── Cargo.lock
└── target
    └── debug
```

???

- Have students list the directory

---

# Hello, world!

```rust
fn main() {
    println!("Hello, world!");
}
```

???

- `fn` keyword
- `main` is entry point for an executable
- parenthesis
- curly braces
- 4 space indent
- println is a macro, don't worry too much about that
- semicolons

---

# Printing values

```rust
fn main() {
    println!("Hello, {}", "alice");
    println!("Hello, {:?}", "bob");
}
```

???

- `{}` prints something intended for an end-user
- `{:?}` prints something for a developer

---

# Comments

```rust
fn main() {
    // println!("Hello, {}", "alice");
    /* println!("Hello, {:?}", "bob"); */
}
```

---

class: bullets-7x7

# API Documentation

- https://doc.rust-lang.org/
  - "The Standard Library"
- `rustup doc`
  - `rustup doc --std`

---

# Functions

```rust
fn the_name(arg1: Type1, arg2: Type2, ...) -> ReturnType {
    statement;
    statement;
    expression
}
```

???

- statement has a semicolon at the end
- expression doesn't
- a block can have many statements, last may be expression
- final expression of a block is the value of the block
- Return type is optional

--

```rust
fn the_name(arg1: Type1, arg2: Type2, ...) -> ReturnType {
    return expression;
}
```

???

- Can use `return` to exit from a function early

---

# Variables

```rust
let age = 21;
let is_enabled = true;
let emoji = '🌍';
```

???

- keyword `let`
- every variable has a type
- type is inferred

---

# Variables

```rust
fn main() {
    let age = 21;
    age += 1;
}
```

???

- Try this

---

# Variables are immutable by default

```rust
fn main() {
    let `mut` age = 21;
    age += 1;
}
```

???

- Lots of functional languages do this
- Reduces places to reason about accidental changes
- Ever passed a collection to a function and it modified it?
- Default encourages usage
- Possible for the compiler to make some optimizations

---

class: bullets-7x7

# Built-in types

- `u32`: unsigned 32-bit integer
- `i32`: signed 32-bit integer
- `f64`: floating point number
- `String` and/or `&str`: more on these later
- `bool`: a boolean
- `(T1, T2, ...)`: a tuple

???

- Different sizes of numeric types (`i32`, `u8`, etc.)
- `Vec` / `[T]` / `[T; N]`

---

class: bullets-7x7

# Type inference / explicit types

- Most of the time, you don't need to specify the type
- You can choose to if it helps you learn

```rust
let name: Type = value;
```

???

- Can use the empty tuple to get a compile error with the real type

---

# `if`

```rust
if condition {
    // statements + optional expression
} else if another_condition {
    // statements + optional expression
} else {
    // statements + optional expression
}
```

???

- No parenthesis on condition
- Evaluates to value of chosen block

---

# `match`

```rust
match expression {
    pattern_1 => /* expression */,
    pattern_2 => {
        // statements + optional expression
    }
    _ => /* expression */,
}
```

???

- can have multiple patterns
- first that matches is applied
- can have single expression or curly braces
- catch-all pattern of `_`

---

class: bullets-7x7

# Exercise

- Create a `fibonacci` function
  - `F(n) = F(n-1) + F(n-2)`
  - `F(0) = 0`
  - `F(1) = 1`
- Print out the result of calling the function with `10`




---

# One answer

```rust
fn fibonacci(a: u32) -> u32 {
    if a == 0 {
        0
    } else if a == 1 {
        1
    } else {
        fibonacci(a - 1) + fibonacci(a - 2)
    }
}

fn main() {
    println!("{} -> {}", 10, fibonacci(10));
}
```

---

# Another answer

```rust
fn fibonacci(a: u32) -> u32 {
    match a {
        0 => 0,
        1 => 1,
        _ => fibonacci(a - 1) + fibonacci(a - 2),
    }
}

fn main() {
    println!("{} -> {}", 10, fibonacci(10));
}
```

---

# Vectors

```rust
let mut scores = vec![100, 90, 85];
scores[0] -= 10;
scores.push(100);
println!("scores: {:?}", scores);
```

???

- Declare using the `vec!` macro
- Access an item with `[]`
- Lots of methods available; check API for `Vec`
- Rust also has arrays, but they are fairly limited compared to `Vec`

---

# `for`

```rust
for pattern in expression {
     // use variables created in pattern
}
```

???

- `expression` can be anything that creates an iterator

--

Expressions can be things like:

- `0..10` or `0..=9`
- `vector.iter()`
- `&vector`

---

# Iterating

```rust
let scores = vec![100, 90, 85];
```

```rust
for score in scores {
    println!("score: {}", score);
}
```

???

- Have students try to do the loop twice.

--

```rust
for score in &scores {
    println!("score: {}", score);
}
```

???

- Iterating over `scores` takes ownership, `&scores` does not

---

# Iterators

```rust
for i in 0..10 {
    println!("{}", i);
}
```

???

- Iterator adapters like `map` or `filter` produce new iterators
- Check the docs for `Iterator`

--

```rust
for i in (0..10).map(|x| x * 2) {
    println!("{}", i);
}
```

--

```rust
for i in (0..10).filter(|y| y % 2 == 0).map(|x| x * 2) {
    println!("{}", i);
}
```

---

# Iterators

```rust
let values: Vec<i32> = (0..10).collect();
```

???

- Iterator consumers convert to something else

--

```rust
let values: i32 = (0..10).sum();
```

---

class: bullets-7x7

# Exercise

- Print out the values from 0 (inclusive) to 100 (exclusive)
- That are divisible by 3
- And divisible by 7

--

<hr />

- Instead of printing them out, try adding them up

---

# One answer

```rust
fn main() {
    let iter = (0..100)
        .filter(|y| y % 3 == 0)
        .filter(|y| y % 7 == 0);

    for i in iter {
        println!("{}", i);
    }
}
```

---

# Another answer

```rust
fn main() {
    for i in (0..100).filter(|y| y % 3 == 0 && y % 7 == 0) {
        println!("{}", i);
    }
}
```

---

# Structs

```rust
struct TypeName {
    member_name: MemberType,
}
```

```rust
let val = TypeName {
    member_name: member_value,
};

val.member_name;
```

???

- structs allow us to group related data together
- A few different ways of defining a struct, but this is the base case

---

# Enums

```rust
enum TypeName {
    VariantOne,
    VariantTwo(Type1),
}
```

```rust
let val_1 = TypeName::VariantOne;
let val_2 = TypeName::VariantTwo(some_value);

match val_2 {
    TypeName::VariantOne => println!("one"),
    TypeName::VariantTwo(v) => println!("two {}", v),
}
```

???

- enums allow us to have mutually exclusive *variants*
- often used with a `match` statement
- likewise, there are a few ways to define each variant, but this is most common

---

class: bullets-7x7

# Exercise

- Create `Fahrenheit` and `Celcius` structs
- Create a function that converts `Fahrenheit` to `Celcius`
- `C = (F - 32.0) / 1.8`

--

<hr />

- Instead of a struct, do it with a single enum

---

# One answer

```rust
struct Farenheit {
    value: f64,
}

struct Celcius {
    value: f64,
}

fn farenheit_to_celcius(temperature: Farenheit) -> Celcius {
    Celcius {
        value: (temperature.value - 32.0) / 1.8,
    }
}

fn main() {
    let f = Farenheit { value: 70.0 };
    let c = farenheit_to_celcius(f);
    println!("is {:?}", c.value);
}
```

???

- derive - Debug, Copy?

---

# Methods

```rust
impl TypeName {
    fn method_1(`self`, arg1: Type1) -> ReturnType {
        // ...
    }
}
```

???

- methods allow us to associate functions with a specific set of data (struct or enum)

--

```rust
impl TypeName {
    fn method_2(`&self`, arg1: Type1) -> ReturnType {
        // ...
    }
}
```

--

```rust
impl TypeName {
    fn method_3(`&mut self`, arg1: Type1) -> ReturnType {
        // ...
    }
}
```

---

class: bullets-7x7

# Exercise

- Create a method that converts `Celcius` to `Fahrenheit`
- `F = C * 1.8 + 32.0`

---

# An answer

```rust
struct Farenheit {
    value: f64,
}

struct Celcius {
    value: f64,
}

impl Celcius {
    fn to_farenheit(&self) -> Farenheit {
        Farenheit {
            value: (self.value - 32.0) / 1.8,
        }
    }
}

fn main() {
    let c = Celcius { value: 100.0 };
    let f = c.to_farenheit();
    println!("is {:?}", f.value);
}
```

---

class: bullets-7x7

# Strings

- Rust has two primary string types:
- `String`
  - Owns the data
  - Can be extended or reduced
- `&str`
  - References existing data
  - Cannot change length

---

class: bullets-7x7

# Strings

- Can convert from a `&str` to a `String` via `to_string()`

```rust
"hello".to_string()
```

- Can get a `&str` from a `String` via `as_str()`

```rust
String::new().as_str()
```

???

- Can use `to_string` to convert lots of types to a String, actually

---

class: bullets-7x7

# Exercise

- Create a function that prints a number
- Multiples of three print “Fizz” instead of the number
- Multiples of five print “Buzz” instead of the number
- Multiples of both three and five print “FizzBuzz”
- Call the function with the numbers from 1 to 100

--

<hr />

- Change the function to return a string instead of printing

---

# One answer

```rust
fn fizz_buzz(i: u32) -> String {
    match (i % 3 == 0, i % 5 == 0) {
        (true, true) => "FizzBuzz".to_string(),
        (true, false) => "Fizz".to_string(),
        (false, true) => "Buzz".to_string(),
        (false, false) => i.to_string(),
    }
}

fn main() {
    for i in 1..=100 {
        println!("{}", fizz_buzz(i));
    }
}
```

---

# Reading user input

```rust
use std::io::prelude::*;
use std::io;

fn main() {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)
        .expect("Couldn't read from stdin");
    println!("Read: {:?}", input);
}
```

???

- Warning: the string has the trailing newline from hitting enter!
- Use `trim` to remove it.
- `expect` is for error handling; will talk more in a second

---

# Parsing strings

```rust
fn main() {
    let a = "42".parse::<i32>().expect("Not an integer");
}
```

???

- Turbofish specifies what target type

---

class: bullets-7x7

# Exercise

- Read user input of a temperature and convert it

--

<hr />

- Ask if it's Celcius or Fahrenheit

---

class: bullets-7x7

# Handling errors

- Rust does not have exceptions
- You can:
  - return an error
  - panic

---

class: bullets-7x7

# Returning errors

- `Result` is an enum

    ```rust
    enum Result<T, E> {
        Ok(T),
        Err(E),
    }
    ```

???

- Generic types for success and failure cases
- Aside: `Option<T>`

---

class: bullets-7x7

# Chained error returns

The `?` operator is syntax sugar for returning an error or getting the success value.

```rust
fn may_error() -> Result<i32, bool> { /* */ }

fn calls_may_error() -> Result<i32, bool> {
    let val = may_error()?;
    Ok(val * 2)
}
```

---

class: bullets-7x7

# Panicking

- Tears down the current thread
  - If it's the main thread, program exits
- Safe to do, in Rust terms
- When to panic:
  - Great for prototyping and "learning Rust" workshops
  - OK for an executable
  - Not good for a library
      - Unless there's an error from the library writer

---

# Explicit panics

```rust
panic!("This shouldn't happen");
unreachable!("This code can't be run but it is");
unimplemented!("This code hasn't been written yet");
```

---

class: bullets-7x7

# Implicit panics

- `Option::unwrap()` / `Result::unwrap()`
- `Option::expect("reason")` / `Result::expect("reason")`
- Indexing out of bounds (`foo[1000]`)

---

class: bullets-7x7

# Exercise

- Write a function that adds two `i32` values
- If either of the values are greater than `10`, return an error
- If the sum is greater than `15`, return an error
- Call the function and panic if it fails

## Hints

- Write a helper function for the repeated logic and use `?`
- Use `()` as your returned error type and its value

---

# One answer

```rust
fn acceptable_number(a: i32, max: i32) -> Result<i32, ()> {
    if a > max {
        Err(())
    } else {
        Ok(a)
    }
}

fn strange_adder(a: i32, b: i32) -> Result<i32, ()> {
    let c = acceptable_number(a, 10)? +
            acceptable_number(b, 10)?;
    acceptable_number(c, 15)
}

fn main() {
    strange_adder(10, 10).expect("Oh no");
}
```

---

# Ownership

```rust
fn do_tough_work() {
    let mut powers = vec![1, 2, 4, 8];
}
```

???

- Owner controls mutability
- When owner goes out of scope, it's cleaned up

---

# Borrowing

```rust
let knowledge = Wikipedia::download();
let a_reference_to_knowledge = &knowledge;
```

---

# Borrowing

```rust
let knowledge = Wikipedia::download();
let a_reference_to_knowledge = `&`knowledge;
```

???

- All thanks to the humble ampersand
- `knowledge` is something we don't want to duplicate needlessly
- Maybe it takes a lot of memory?
- `a_reference_to_knowledge` points to the same memory
- It's not a copy, so it's very lightweight

---

# Ownership and borrowing

```rust
let knowledge = Wikipedia::download();
let a_reference_to_knowledge = &knowledge;
```

<img class="full-image" src="./assets/value.png" alt="value analogy" />

---

# Ownership and borrowing

```rust
let knowledge = Wikipedia::download();
let a_reference_to_knowledge = &knowledge;
```

<img class="full-image" src="./assets/reference.png" alt="reference analogy" />

---

# Immutable and mutable borrowing

```rust
let a_book = String::new();
let reader = `&`a_book;
```

```rust
let `mut` a_book = String::new();
let author = `&mut` a_book;
```

---

# Immutable and mutable borrowing

```rust
let mut a_book = String::new();
let author = &mut a_book;
let reader = &a_book;
```

<pre class="compiler-error">
error[E0502]: cannot borrow `a_book` as immutable because it
              is also borrowed as mutable
  |
3 | let author = &mut a_book;
  |                   ------ mutable borrow occurs here
4 | let reader = &a_book;
  |               ^^^^^^ immutable borrow occurs here
5 | }
  | - mutable borrow ends here
</pre>

???

- If you have a mutable reference, no other references of any kind
- If you have immutable references, as many as you want
- Definitely cannot have both at the same time

---

# Slices

```rust
let scores = vec![1, 2, 3];
let some_scores = &scores[1..];
```

???

- Special kind of reference

---

# Slices

<img class="full-image" src="./assets/vec.png" alt="memory of Vec" />

---

# Slices

<img class="full-image" src="./assets/slice.png" alt="memory of slice" />


???

- Points to the start of some data and knows how many items there are
- Not *that* unique, but the usability and safety comes from borrowing

---

# String slices

```rust
let novel = "hello, world!;
let chapter_1 = &novel[..5];
```

???

- Special kind of slice
- Always guaranteed to be UTF-8

---

# String slices

<img class="full-image" src="./assets/string.png" alt="memory of String" />

---

# String slices

<img class="full-image" src="./assets/string-slice.png" alt="memory of &str" />

---

# The borrow checker

```rust
let a_reference_to_the_book = {
    let a_book = String::new();
    &a_book
};
```

<pre class="compiler-error">
error[E0597]: `a_book` does not live long enough
  |
4 |     &a_book
  |      ^^^^^^ borrowed value does not live long enough
5 | };
  | - `a_book` dropped here while still borrowed
6 |
7 | }
  | - borrowed value needs to live until here
</pre>

???

- We take a reference to the same book, but it is destroyed before we use it
- This is caught at compile time
- No dangling pointers (and all the security holes)

---

# Move semantics

```rust
let a_book = String::new();
let ref_to_the_book = &a_book;
let a_moved_book = a_book;
```

<pre class="compiler-error">
error[E0505]: cannot move out of `a_book` because
              it is borrowed
  |
3 | let ref_to_the_book = &a_book;
  |                        ------ borrow of `a_book`
  |                               occurs here
4 | let a_moved_book = a_book;
  |     ^^^^^^^^^^^^ move out of `a_book` occurs here
</pre>

???

- The previous example (being dropped) was a special case of a move
- An upcoming change to the language will allow this because this specific code is safe

---

# Lifetimes

```rust
fn a_chapter_of_the_book<`'a`>(book: &`'a` str) -> &`'a` str {
    &book[100..]
}

let an_entire_book = String::new();
let a_chapter = a_chapter_of_the_book(&an_entire_book);
```

???

- Lifetimes go hand-in-hand with borrow checker
- Generic lifetimes let us connect input and output references
- These lifetimes are superfluous, just for demonstration

---

# Modules

```rust
mod example {
    fn in_module() {
        println!("I'm inside a module");
    }
}

// accessed as example::in_module
```

???

- `mod` keyword
- Probably getting a warning about an unused function? Go ahead and call it.

---

# Visibility

```rust
pub fn call_me() { /* ... */ }

pub struct Monster {
    pub hit_points: u8,
}

pub enum State {
    OnFire,
    Electrified,
}

pub mod example { /* ... */ };
```

???

- `pub` basically means "the thing that contains me can see this"
- There's some other nuanced visibility, but we don't need to worry about it for now

---

# Modules in files

**src/main.rs**

```rust
mod example {
    pub fn in_module() {
        println!("I'm inside a module");
    }
}
```

???

- This seems to be a complicated aspect for many people.

---

# Modules in files

**src/main.rs**

```rust
mod example;
```

**src/example.rs**

```rust
pub fn in_module() {
    println!("I'm inside a module");
}
```

???

- The next level of nesting is expanding soon (Rust 1.30 or 1.31)

---

# Exercise

- Create a function in a module which calls two others.
- Each function returns a number.
- The parent function should be called in `main`

```rust
fn main() {
    println!("{}", secret::code());
    // secret::key(); // Has an error!
    // secret::signature(); // Has an error!
}
```

---

# One answer

```rust
fn main() {
    println!("{}", secret::code());
    // secret::key(); // Has an error!
    // secret::signature(); // Has an error!
}

mod secret {
    pub fn code() -> i32 {
        key() + signature()
    }

    fn key() -> i32 {
        1
    }

    fn signature() -> i32 {
        2
    }
}
```

---

class: bullets-7x7

# Extra topics

- Rust 2018
- Traits
- Generics
- FFI
- Unsafe
- Closures
- Concurrency
- Crates
